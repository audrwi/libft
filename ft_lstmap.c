/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/04 23:51:37 by aboucher          #+#    #+#             */
/*   Updated: 2015/12/05 19:27:00 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list	*new;
	t_list	*res;
	t_list	*result;

	new = (*f)(lst);
	res = ft_lstnew(new->content, new->content_size);
	result = res;
	if (lst->next)
	{
		lst = lst->next;
		while (lst)
		{
			new = (*f)(lst);
			res->next = ft_lstnew(new->content, new->content_size);
			res = res->next;
			lst = lst->next;
		}
	}
	return (result);
}
