/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/25 11:46:02 by aboucher          #+#    #+#             */
/*   Updated: 2015/12/06 03:09:14 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnstr(const char *s1, const char *s2, size_t n)
{
	size_t	i;
	int		j;
	size_t	start;

	i = 0;
	j = 0;
	start = i;
	if (!s2[j])
		return ((char *)s1);
	while (s1[i] && s2[j] && i < n)
	{
		if (s1[i] == s2[j])
		{
			if (s1[i - 1] != s2[j - 1])
				start = i;
			j++;
		}
		else
		{
			i = start++;
			j = 0;
		}
		i++;
	}
	return (!s2[j] && j != 0 ? (char *)&s1[i - j] : NULL);
}
