/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memset.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/25 21:50:06 by aboucher          #+#    #+#             */
/*   Updated: 2015/11/29 16:46:58 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memset(void *b, int c, size_t len)
{
	size_t			i;
	unsigned char	*str;
	unsigned char	n;

	i = 0;
	str = (unsigned char *)b;
	n = (unsigned char)c;
	while (i < len)
	{
		str[i] = n;
		i++;
	}
	return (str);
}
